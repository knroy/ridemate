package me.knroy.ridemate.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import me.knroy.ridemate.sessionmanager.SessionManager;

public class RidemateFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = RidemateFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        SessionManager.getInstance().setRegId(refreshedToken);
    }
}