package me.knroy.ridemate.services;

import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import me.knroy.ridemate.utils.NotificationUtils;


public class RidemateFireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = RidemateFireBaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null) return;

        if (remoteMessage.getNotification() != null) {
            // testing firebase crash reporting log
            FirebaseCrash.log(remoteMessage.getNotification().getBody());

            handleNotifications(remoteMessage.getNotification().getBody());
        }
    }

    private void handleNotifications(String message) {
        Log.d(TAG, "handleNotifications: " + message);
        if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();
        }else{
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.showNotificationOnActive("Ridemate", message);
        }
    }

}
