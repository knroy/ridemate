package me.knroy.ridemate.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import me.knroy.ridemate.R;
import me.knroy.ridemate.models.ItemPlatform;

/**
 * Created by knroy on 6/18/17.
 * Don't edit or customize without proper privileges
 */

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.SingleTagViewHolder>{

    private static final String TAG = TagsAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<ItemPlatform> platforms;

    private OnTagClickListener listener;

    public TagsAdapter(Context context, ArrayList<ItemPlatform> platforms) {
        this.context = context;
        this.platforms = platforms;
    }

    public void setOnTagClickListener(OnTagClickListener listener){
        this.listener = listener;
    }

    @Override
    public SingleTagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.platform_item,parent,false);
        return new SingleTagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SingleTagViewHolder holder, int position) {
        holder.tagItem.setText(platforms.get(position).getPlatform());
        final int pos = position;
        holder.tagItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(pos);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.platforms.size();
    }

    class SingleTagViewHolder extends RecyclerView.ViewHolder{
        TextView tagItem;
        SingleTagViewHolder(View itemView) {
            super(itemView);
            tagItem = (TextView) itemView.findViewById(R.id.tag_item);
        }
    }

    public interface OnTagClickListener{
        void onClick(int pos);
    }


}
