package me.knroy.ridemate.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import me.knroy.ridemate.R;
import me.knroy.ridemate.exviews.AnimatedLinearLayout;
import me.knroy.ridemate.exviews.SquareImageView;
import me.knroy.ridemate.models.ClientItem;
import me.knroy.ridemate.models.ItemPlatform;

/**
 * Created by knroy on 6/13/17.
 * Don't edit or customize without proper privileges
 */

public class ClientsListAdapter extends RecyclerView.Adapter<ClientsListAdapter.ViewHolder>{

    private static final String TAG = ClientsListAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<ClientItem> items;
    private SparseBooleanArray array;
    private OnClientItemClickListener listener;

    public ClientsListAdapter(Context context, ArrayList<ClientItem> items) {
        this.context = context;
        this.items = items;
        array = new SparseBooleanArray();
    }

    public void setListener(OnClientItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.client_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.clientTitle.setText(items.get(position).getTitle());
        holder.clientName.setText(items.get(position).getClientName());
        holder.country.setText(items.get(position).getCountry());

        ArrayList<ItemPlatform> platforms = items.get(holder.getAdapterPosition()).getPlatforms();
        TagsAdapter adapter = new TagsAdapter(context, platforms);
        adapter.setOnTagClickListener(new TagsAdapter.OnTagClickListener() {
            @Override
            public void onClick(int pos) {
                if(listener != null){
                    listener.onPlatformItemClick(position, pos);
                }
            }
        });

        holder.innerRecycler.setHasFixedSize(true);
        holder.innerRecycler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        holder.innerRecycler.setAdapter(adapter);
        holder.innerRecycler.setNestedScrollingEnabled(false);

        if (items == null)
            return;

        holder.platforms.measureHeight();

        if (array.get(position)) {
            holder.platforms.setSlider(true);
            holder.platforms.setVisibility(View.VISIBLE);
        } else {
            holder.platforms.setVisibility(View.GONE);
            holder.platforms.setSlider(true);
        }

        // higher the value, slow down the transition speed
        holder.platforms.setSpeed(2);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TransitionManager.beginDelayedTransition(holder.container);
                holder.platforms.startAnimating();
                if (array.get(position)) {
                    array.put(position,false);
                } else {
                    array.put(position,true);
                }
            }
        });

        Glide.with(context).load(items.get(position).getDrawableId()).bitmapTransform(new CropCircleTransformation(context)).into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        SquareImageView icon;
        CardView container;
        TextView clientTitle;
        TextView clientName;
        TextView country;
        AnimatedLinearLayout platforms;
        RecyclerView innerRecycler;

        private ViewHolder(View itemView) {
            super(itemView);

            this.clientTitle = (TextView) itemView.findViewById(R.id.client_title);
            this.clientName = (TextView) itemView.findViewById(R.id.client_name);
            this.container = (CardView) itemView.findViewById(R.id.client_item_container);
            this.icon = (SquareImageView) itemView.findViewById(R.id.client_image);
            this.country = (TextView) itemView.findViewById(R.id.client_county);
            this.platforms = (AnimatedLinearLayout) itemView.findViewById(R.id.platforms);
            this.innerRecycler = (RecyclerView) itemView.findViewById(R.id.inner_item);
        }
    }

    public interface OnClientItemClickListener {
        void onPlatformItemClick(int itemPos, int tagPos);
    }

}
