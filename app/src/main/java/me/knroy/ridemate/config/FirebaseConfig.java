package me.knroy.ridemate.config;


public class FirebaseConfig {

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String PUSH_NOTIFICATION  = "firebase_push";

}
