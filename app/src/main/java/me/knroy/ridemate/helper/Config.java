package me.knroy.ridemate.helper;

/**
 * Created by knroy on 6/18/17.
 * Don't edit or customize without proper privileges
 */

public class Config {

    private static final String API_URL = "http://audacityit.com/profile/api/v2/client";
    private static final String VAR_EKY = "authorization";
    private static final String VAR_VALUE = "32DFCFD@#&DSFDSFSDF!L@?hh7@32DF";

    private static Config mInstance;

    private Config(){

    }

    public static Config getInstance(){
        if(mInstance == null){
            mInstance = new Config();
        }
        return mInstance;
    }

    public String getAPIUrl(){
        return API_URL;
    }

    public String getKey(){
        return VAR_EKY;
    }

    public String getValue(){
        return VAR_VALUE;
    }

}
