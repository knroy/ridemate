package me.knroy.ridemate.utils;

import android.content.Context;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import me.knroy.ridemate.models.ClientItem;
import me.knroy.ridemate.models.ItemPlatform;

/**
 * Created by knroy on 6/14/17.
 * Don't edit or customize without proper privileges
 */

public class LocalJson {

    private static LocalJson mInstance;

    private LocalJson() {

    }

    public static LocalJson getInstance() {
        if (mInstance == null) {
            mInstance = new LocalJson();
        }
        return mInstance;
    }

    public static void onDestroy() {
        if (mInstance != null) {
            mInstance = null;
        }
    }

    public ArrayList<ClientItem> getClientItems(Context context, String data) {
        ArrayList<ClientItem> items = new ArrayList<>();
        if (data != null) {

            try {

                JSONObject object = new JSONObject(data);
                if (!object.getBoolean("error")) {

                    JSONArray array = object.getJSONArray("client");
                    int len = array.length();
                    for (int i = 0; i < len; i++) {

                        JSONObject cObject = array.getJSONObject(i);
                        String title = cObject.getString("company");
                        String name = cObject.getString("name");
                        String country = cObject.getString("country");
                        String image = cObject.getString("logo");

                        JSONArray pArray = cObject.getJSONArray("tags");

                        Log.d("TAG", "getClientItems: " + pArray.length());

                        ArrayList<ItemPlatform> platforms = new ArrayList<>();

                        for (int j = 0; j < pArray.length(); j++) {
                            platforms.add(new ItemPlatform(pArray.getJSONObject(j).getString("tag"), pArray.getJSONObject(j).getString("url")));
                        }

                        items.add(
                                ClientItem.builder()
                                        .withTitle(title)
                                        .withClientName(name)
                                        .withCountry(country)
                                        .withDrawableId(image)
                                        .withPlatforms(platforms)
                                        .build()
                        );
                    }
                }

            } catch (JSONException e) {
                // sending crash report to FirebaseCrash
                FirebaseCrash.report(new Exception(e.getMessage()));
                //e.printStackTrace();
            }

        }

        return items;
    }
}
