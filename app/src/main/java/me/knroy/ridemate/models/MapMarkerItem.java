package me.knroy.ridemate.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by knroy on 6/13/17.
 * Don't edit or customize without proper privileges
 */

public class MapMarkerItem {
    private LatLng latLng;
    private int markerTag;
    private int travelTime;

    private MapMarkerItem(Builder builder){
        this.latLng = builder.latLng;
        this.markerTag = builder.markerTag;
        this.travelTime = builder.travelTime;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{

        private LatLng latLng;
        private int markerTag;
        private int travelTime;

        public Builder withLatLng(LatLng latLng){
            this.latLng = latLng;
            return this;
        }

        public Builder withMarkerTag(int markerTag){
            this.markerTag = markerTag;
            return this;
        }

        public Builder withTravelTime(int travelTime){
            this.travelTime = travelTime;
            return this;
        }

        public MapMarkerItem build(){
            return new MapMarkerItem(this);
        }
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public int getMarkerTag() {
        return markerTag;
    }

    public int getTravelTime() {
        return travelTime;
    }
}
