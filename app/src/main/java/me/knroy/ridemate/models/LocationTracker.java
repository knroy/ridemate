package me.knroy.ridemate.models;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import me.knroy.ridemate.activities.map.LocationDataListener;


/**
 * Created by knroy on 6/11/17.
 * Don't edit or customize without proper privileges
 */

public class LocationTracker extends Service implements LocationListener {

    private static final String TAG = LocationTracker.class.getSimpleName();

    private static final float MIN_DISTANCE_UPDATE = 10; // 10 metre
    private static final long MIN_TIME_UPDATE = 1000 * 60;

    private Context mContext;
    private Location mLocation;
    private LocationManager mLocationManager;

    private boolean isLocationTrackingEnabled;
    private boolean isNetworkAvailable;

    private boolean locationAvailable;

    private LatLng latLng;

    private boolean isFirstTime;

    private LocationDataListener mListener;

    public LocationTracker(Context mContext) {
        this.mContext = mContext;
        locationAvailable = false;
        isFirstTime = true;
    }

    public void start(boolean requestNeeded) {
        if (isPermitted()) {
            startPrerequisite();
            Log.d(TAG, "start: " + "started Prerequisite");
            if (isLocationTrackingEnabled || isNetworkAvailable) {
                networkLocationTrace();
                GPSProviderLocationTrace();
            }else if(!canGetLocation()){
                if(mListener!= null && requestNeeded){
                    mListener.requestGpsEnable();
                }
            }
        } else {
            if (mListener != null) {
                mListener.requestPermission();
            }
        }
    }

    private void startPrerequisite() {
        try {
            mLocationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
            isLocationTrackingEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkAvailable = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void networkLocationTrace() {
        if (isNetworkAvailable) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_UPDATE, MIN_DISTANCE_UPDATE, this);
            if (mLocationManager != null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (mLocation != null) {
                    latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                    this.locationAvailable = true;
                }
            }
        }
        Log.d(TAG, "networkLocationTrace: Location Available" + locationAvailable);
    }

    private void GPSProviderLocationTrace() {
        if (isLocationTrackingEnabled) {
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_UPDATE, MIN_DISTANCE_UPDATE, this);
            if (mLocationManager != null) {
                mLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (mLocation != null) {
                    latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                    this.locationAvailable = true;
                }
            }
        }
        Log.d(TAG, "networkLocationTrace: Location Available" + locationAvailable);
    }


    @Override
    public void onDestroy() {
        try {
            mLocationManager.removeUpdates(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        stopSelf();
    }

    public void setListener(LocationDataListener mListener) {
        this.mListener = mListener;
    }

    private boolean isPermitted() {
        return ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public LatLng getLatLng() {
        return this.latLng;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location mLocation) {
        this.mLocation = mLocation;
        latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        locationAvailable = true;
        if (mListener != null && isFirstTime) {
            isFirstTime = false;
            mListener.onLocationChange(latLng);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public boolean canGetLocation() {
        return locationAvailable;
    }

    public void onResume(){
        start(false);
    }
}
