package me.knroy.ridemate.models;

/**
 * Created by knroy on 6/14/17.
 * Don't edit or customize without proper privileges
 */

public class ItemPlatform {
    private String platform;
    private String url;

    public ItemPlatform(String platform, String url) {
        this.platform = platform;
        this.url = url;
    }

    public String getPlatform() {
        return platform;
    }

    public String getUrl() {
        return url;
    }
}
