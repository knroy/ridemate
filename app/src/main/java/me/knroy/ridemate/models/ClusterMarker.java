package me.knroy.ridemate.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by knroy on 6/26/17.
 * Don't edit or customize without proper privileges
 */

public class ClusterMarker implements ClusterItem{

    private final String mTitle;
    private final LatLng mPosition;
    private final String mSnippet;

    public ClusterMarker(String mTitle, LatLng mPosition, String mSnippet) {
        this.mTitle = mTitle;
        this.mPosition = mPosition;
        this.mSnippet = mSnippet;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }
}
