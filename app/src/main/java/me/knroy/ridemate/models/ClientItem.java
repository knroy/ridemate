package me.knroy.ridemate.models;

import java.util.ArrayList;

/**
 * Created by knroy on 6/13/17.
 * Don't edit or customize without proper privileges
 */

public class ClientItem {

    private String title;
    private String clientName;
    private String drawableId;
    private String country;
    private ArrayList<ItemPlatform> platforms;

    private ClientItem(Builder builder){
        this.title = builder.title;
        this.clientName = builder.clientName;
        this.drawableId = builder.drawableId;
        this.country = builder.country;
        this.platforms = builder.platforms;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder{
        private String title;
        private String clientName;
        private String drawableId;
        private String country;
        private ArrayList<ItemPlatform> platforms;

        public Builder withTitle(String title){
            this.title = title;
            return this;
        }

        public Builder withClientName(String clientName){
            this.clientName = clientName;
            return this;
        }

        public Builder withCountry(String country){
            this.country = country;
            return this;
        }

        public Builder withDrawableId(String drawableId){
            this.drawableId = drawableId;
            return this;
        }

        public Builder withPlatforms(ArrayList<ItemPlatform> platforms){
            this.platforms = platforms;
            return this;
        }

        public ClientItem build(){
            return new ClientItem(this);
        }
    }

    public String getTitle() {
        return title;
    }

    public String getClientName() {
        return clientName;
    }

    public String getDrawableId() {
        return drawableId;
    }

    public String getCountry() {
        return country;
    }

    public ArrayList<ItemPlatform> getPlatforms() {
        return platforms;
    }
}
