package me.knroy.ridemate.fragments.home;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import me.knroy.ridemate.R;
import me.knroy.ridemate.activities.map.ConfirmationActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements HomeView, View.OnClickListener{

    private HomePresenterImpl presenter;

    public Home() {
        // Required empty public constructor
    }

    public static Home getInstance(){
        return new Home();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_home, container, false);

        Button mapTask = (Button) view.findViewById(R.id.map_task);
        mapTask.setOnClickListener(this);

        presenter = new HomePresenterImpl(this);

        return view;
    }

    @Override
    public void onButtonClick() {
        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        presenter.startMapTask();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }
}
