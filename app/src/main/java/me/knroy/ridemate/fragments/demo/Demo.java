package me.knroy.ridemate.fragments.demo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import me.knroy.ridemate.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Demo extends Fragment {

    private static final String KEY_TEXT = "text";

    private TextView titleText;

    public Demo() {
        // Required empty public constructor
    }

    public static Demo getInstance(String text){
        Demo demo = new Demo();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_TEXT,text);
        demo.setArguments(bundle);
        return demo;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_demo, container, false);
        titleText = (TextView) view.findViewById(R.id.text);

        if(getArguments() != null){

            Bundle bundle = getArguments();
            String text = bundle.getString(KEY_TEXT);
            this.titleText.setText(text);
        }

        return view;
    }

}
