package me.knroy.ridemate.fragments.home;

import android.os.Handler;

/**
 * Created by knroy on 6/7/17.
 * Don't edit or customize without proper privileges
 */

public class HomePresenterImpl implements HomePresenter{

    private HomeView homeView;

    public HomePresenterImpl(HomeView homeView) {
        this.homeView = homeView;
    }

    @Override
    public void startMapTask() {
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                homeView.onButtonClick();
            }
        },500);
    }

    @Override
    public void onDestroy() {
        homeView = null;
    }
}
