package me.knroy.ridemate.fragments.clients;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import me.knroy.ridemate.R;
import me.knroy.ridemate.adapters.ClientsListAdapter;
import me.knroy.ridemate.models.ClientItem;


public class Clients extends Fragment implements ClientsView, ClientsListAdapter.OnClientItemClickListener {

    private static final String TAG = Clients.class.getSimpleName();

    private TextView emptyText;

    private RecyclerView clientsList;
    private ClientsPresenterImpl presenter;
    private ClientsListAdapter adapter;

    private ArrayList<ClientItem> items;

    private MaterialDialog dialog;

    private RelativeLayout transitionContainer;

    public Clients() {

    }

    public static Clients getInstance() {
        return new Clients();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_clients, container, false);

        emptyText = (TextView) view.findViewById(R.id.empty_text);
        clientsList = (RecyclerView) view.findViewById(R.id.clientsList);
        transitionContainer = (RelativeLayout) view.findViewById(R.id.trans_container);

        presenter = new ClientsPresenterImpl(this);
        presenter.init();
        presenter.loadClientsJson(getActivity());

        return view;
    }

    @Override
    public void showProgressBar() {
        if (!dialog.isShowing())
            dialog.show();
    }

    @Override
    public void hideProgressBar() {
        if (dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void setListItems(ArrayList<ClientItem> items) {
        this.items = items;
        adapter = new ClientsListAdapter(getActivity(), this.items);
        adapter.setListener(this);
    }

    @Override
    public void updateListItems(ArrayList<ClientItem> items) {
        this.items.clear();
        this.items.addAll(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void initClientsList() {
        clientsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        clientsList.setHasFixedSize(true);
        clientsList.setNestedScrollingEnabled(false);
        clientsList.setAdapter(adapter);
    }

    @Override
    public void showEmptyView() {
        emptyText.setVisibility(View.VISIBLE);
        clientsList.setVisibility(View.GONE);
    }

    @Override
    public void showClientsListView() {
        clientsList.setVisibility(View.VISIBLE);
        emptyText.setVisibility(View.GONE);
    }

    @Override
    public void initDialog() {
        dialog = new MaterialDialog.Builder(getActivity())
                .content(getString(R.string.loading))
                .progressIndeterminateStyle(true)
                .build();
    }

    @Override
    public void onPlatformItemClick(int itemPos, int tagPos) {
        String url = items.get(itemPos).getPlatforms().get(tagPos).getUrl();
        visitUrl(url);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void visitUrl(String url){

        onResume();

        if(!url.contains("www") && !url.startsWith("www") && !url.startsWith("http")){
            url = "www." + url;
        }

        if(!url.contains("http") && !url.startsWith("http")){
            url = "http://"  + url;
        }

        Log.d(TAG, "visitUrl: " + url);

        Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        Intent chooser = Intent.createChooser(sendIntent, "Choose Your Browser");
        Log.d(TAG, "visitUrl: " + url);
        if (sendIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivity(chooser);
        }
    }

}
