package me.knroy.ridemate.fragments.demo;

/**
 * Created by knroy on 6/7/17.
 * Don't edit or customize without proper privileges
 */

public interface DemoPresenter {
    void setTitleText();
}
