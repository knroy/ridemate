package me.knroy.ridemate.fragments.clients;

import java.util.ArrayList;

import me.knroy.ridemate.models.ClientItem;

/**
 * Created by knroy on 6/14/17.
 * Don't edit or customize without proper privileges
 */

public interface ClientsView {
    void showProgressBar();
    void hideProgressBar();
    void setListItems(ArrayList<ClientItem> items);
    void updateListItems(ArrayList<ClientItem> items);
    void initClientsList();
    void showEmptyView();
    void showClientsListView();
    void initDialog();
}
