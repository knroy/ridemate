package me.knroy.ridemate.fragments.clients;

import android.content.Context;
import android.view.View;

/**
 * Created by knroy on 6/14/17.
 * Don't edit or customize without proper privileges
 */

public interface ClientsPresenter {
    void onDestroy();
    void loadClientsJson(Context context);
    void init();
}
