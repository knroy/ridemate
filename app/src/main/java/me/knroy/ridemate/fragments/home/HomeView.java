package me.knroy.ridemate.fragments.home;

/**
 * Created by knroy on 6/7/17.
 * Don't edit or customize without proper privileges
 */

public interface HomeView {
    void onButtonClick();
}
