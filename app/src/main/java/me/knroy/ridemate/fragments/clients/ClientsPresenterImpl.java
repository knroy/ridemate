package me.knroy.ridemate.fragments.clients;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.knroy.ridemate.helper.Config;
import me.knroy.ridemate.models.ClientItem;
import me.knroy.ridemate.utils.LocalJson;

/**
 * Created by knroy on 6/14/17.
 * Don't edit or customize without proper privileges
 */

public class ClientsPresenterImpl implements ClientsPresenter {

    private static final String TAG = ClientsPresenterImpl.class.getSimpleName();

    private ClientsView clientsView;

    public ClientsPresenterImpl(ClientsView clientsView) {
        this.clientsView = clientsView;
    }

    @Override
    public void loadClientsJson(final Context context) {

        StringRequest request = new StringRequest(Request.Method.GET, Config.getInstance().getAPIUrl(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                clientsView.hideProgressBar();
                Log.d(TAG, "onResponse: " + response);

                ArrayList<ClientItem> list = LocalJson.getInstance().getClientItems(context, response);

                if (list.size() <= 0) clientsView.showEmptyView();
                else clientsView.showClientsListView();
                clientsView.updateListItems(list);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                clientsView.hideProgressBar();
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                headers.put(Config.getInstance().getKey(), Config.getInstance().getValue());
                return headers;
            }
        };

        Volley.newRequestQueue(context).add(request);
    }

    @Override
    public void init() {
        clientsView.showEmptyView();
        clientsView.initDialog();
        clientsView.showProgressBar();
        clientsView.setListItems(new ArrayList<ClientItem>());
        clientsView.initClientsList();
    }

    @Override
    public void onDestroy() {
        this.clientsView = null;
    }
}
