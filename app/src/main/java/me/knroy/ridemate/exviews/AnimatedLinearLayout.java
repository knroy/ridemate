package me.knroy.ridemate.exviews;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;

/**
 * Created by knroy on 6/10/17.
 * Don't edit or customize without proper privileges
 */

public class AnimatedLinearLayout extends LinearLayout implements Animation.AnimationListener {

    private final String TAG = AnimatedLinearLayout.this.getClass().getName();

    private boolean isSlide;

    private OnMenuClosedListener onMenuClosedListener;

    private int height;
    private int speed;

    public AnimatedLinearLayout(Context context) {
        this(context, null);
    }

    public AnimatedLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setSpeed(int speed){
        this.speed = speed;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AnimatedLinearLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setSlider(boolean isSlide){
        this.isSlide = isSlide;
    }

    private void init() {

        height = getMeasuredHeight();
        isSlide = false;
        speed = 1;

    }

    public void startAnimating() {
        Log.d(TAG, "startAnimating: I am calling the animation");
        if (!isSlide) {
            isSlide = true;
            collapse();
        } else {
            isSlide = false;
            expand();
        }
    }

    public void measureHeight(){
        measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        height = getMeasuredHeight();
    }

    public boolean isVisible() {
        return isSlide;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        clearAnimation();
        if (onMenuClosedListener != null)
            onMenuClosedListener.isLayoutVisible(isVisible());
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public void setOnMenuClosedListener(OnMenuClosedListener onMenuClosedListener) {
        this.onMenuClosedListener = onMenuClosedListener;
    }

    public interface OnMenuClosedListener {
        void isLayoutVisible(boolean isVisible);
    }

    private void expand() {
        measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = height;
        getLayoutParams().height = 1;
        setVisibility(View.VISIBLE);
        Animation expandAnimation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                getLayoutParams().height = interpolatedTime == 1 ? WindowManager.LayoutParams.WRAP_CONTENT : (int) (targetHeight * interpolatedTime);
                requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int animDuration = ((int) (targetHeight / getContext().getResources().getDisplayMetrics().density));

        expandAnimation.setDuration(animDuration * speed);
        expandAnimation.setAnimationListener(this);
        startAnimation(expandAnimation);
    }

    private void collapse() {

        final int initialHeight = getMeasuredHeight();
        height = initialHeight;

        Animation collapseAnimation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    setVisibility(View.GONE);
                } else {
                    getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        int animDuration = ((int) (initialHeight / getContext().getResources().getDisplayMetrics().density));

        collapseAnimation.setDuration(animDuration * speed);
        collapseAnimation.setAnimationListener(this);
        startAnimation(collapseAnimation);
    }

}