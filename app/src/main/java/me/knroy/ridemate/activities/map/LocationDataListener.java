package me.knroy.ridemate.activities.map;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by knroy on 6/11/17.
 * Don't edit or customize without proper privileges
 */

public interface LocationDataListener {
    void requestPermission();
    void onLocationChange(LatLng latLng);
    void requestGpsEnable();
}
