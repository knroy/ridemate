package me.knroy.ridemate.activities.main;

import android.support.v4.app.Fragment;

/**
 * Created by knroy on 6/6/17.
 * Don't edit or customize without proper privileges
 */

interface MainView {
    void setActionBarTitle(String title);
    void loadFragment(Fragment fragment);
    void closeDrawer();
    String getActionbarString(int id);
    void onNavCall();
    void onNavMail();
    void onNavVisit();
    void makeToast(String message);
}
