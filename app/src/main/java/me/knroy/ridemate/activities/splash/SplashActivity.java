package me.knroy.ridemate.activities.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.knroy.ridemate.R;
import me.knroy.ridemate.activities.main.MainActivity;
import me.knroy.ridemate.sessionmanager.SessionManager;

public class SplashActivity extends AppCompatActivity implements SplashView{

    private SplashPresenterImpl presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        // starting sessionManager
        SessionManager.start(this);

        this.presenter = new SplashPresenterImpl(this);
        presenter.splash();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void navigateToMain() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
        finish();
    }
}
