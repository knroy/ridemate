package me.knroy.ridemate.activities.main;

/**
 * Created by knroy on 6/7/17.
 * Don't edit or customize without proper privileges
 */

interface MainPresenter {
    void onDestroy();
    void fragmentLoader(int pos);
    void onExtraItemSelected(int id);
}
