package me.knroy.ridemate.activities.map;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import me.knroy.ridemate.R;
import me.knroy.ridemate.models.MapMarkerItem;

/**
 * Created by knroy on 6/8/17.
 * Don't edit or customize without proper privileges
 */

public class ConfirmationPresenterImpl implements ConfirmationPresenter, GoogleMap.OnMarkerClickListener{

    private ConfirmationView cView;

    public ConfirmationPresenterImpl(ConfirmationView cView) {
        this.cView = cView;
    }

    @Override
    public void onDestroy() {
        cView = null;
    }

    // update the pick up point text

    @Override
    public void geoUpdater(Context context, LatLng latLng) {
        if(latLng == null) return;
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            String address = addresses.get(0).getAddressLine(0);
            String city = addresses.get(0).getLocality();
            cView.updateGeo(String.format(Locale.getDefault(),"%s, %s",address,city));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // requesting to change the GPS Location settings, it's needed

    @Override
    public void requestGPSSettings(final Context context) {
        new MaterialDialog.Builder(context)
                .title(R.string.gps_dialog_title)
                .content(R.string.gps_dialog_content)
                .positiveText(R.string.settings)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                context.startActivity(intent);
                            }
                        }, 1000);
                    }
                })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build().show();
    }

    //getting random location from the current position of that user
    private LatLng getRandomLocation(LatLng point, int radius) {

        List<LatLng> randomPoints = new ArrayList<>();
        List<Float> randomDistances = new ArrayList<>();
        Location myLocation = new Location("");
        myLocation.setLatitude(point.latitude);
        myLocation.setLongitude(point.longitude);

        for(int i = 0; i<10; i++) {
            double x0 = point.latitude;
            double y0 = point.longitude;

            Random random = new Random();

            double radiusInDegrees = radius / 111000f;

            double u = random.nextDouble();
            double v = random.nextDouble();
            double w = radiusInDegrees * Math.sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.cos(t);
            double y = w * Math.sin(t);

            double new_x = x / Math.cos(y0);

            double foundLatitude = new_x + x0;
            double foundLongitude = y + y0;
            LatLng randomLatLng = new LatLng(foundLatitude, foundLongitude);
            randomPoints.add(randomLatLng);
            Location l1 = new Location("");
            l1.setLatitude(randomLatLng.latitude);
            l1.setLongitude(randomLatLng.longitude);
            randomDistances.add(l1.distanceTo(myLocation));
        }

        int indexOfNearestPointToCentre = randomDistances.indexOf(Collections.min(randomDistances));
        return randomPoints.get(indexOfNearestPointToCentre);
    }

    private ArrayList<MapMarkerItem> getRandomLatLngs(LatLng point, int radius){
        ArrayList<MapMarkerItem> latLngs = new ArrayList<>();
        for (int i=0;i<5;i++){
            LatLng randLatLng = getRandomLocation(point,radius);
            MapMarkerItem item = MapMarkerItem.builder().withLatLng(randLatLng).withMarkerTag(i).build();
            latLngs.add(item);
        }
        return latLngs;
    }

    // updating the map and adding markers based on current position
    // of the user

    @Override
    public void mapSync(Context context, GoogleMap map, LatLng latLng) {
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setZoomControlsEnabled(true);

        if (latLng == null) return;

        map.clear();

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        map.addMarker(new MarkerOptions().position(latLng).icon(getMarkerIconFromDrawable(context)));

        // dummy markers [based on user location]
        // put random cars upto 500 metre from the user
        ArrayList<MapMarkerItem> latLngs = getRandomLatLngs(latLng,500);

        for (MapMarkerItem ll : latLngs){
            Drawable drawable = ActivityCompat.getDrawable(context,R.drawable.car);
            Marker marker = map.addMarker(new MarkerOptions().position(ll.getLatLng()).icon(getMarkerIconFromDrawable(drawable)));
            marker.setTag(ll.getMarkerTag());
        }

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
        map.animateCamera(cameraUpdate);
        map.setOnMarkerClickListener(this);
    }

    // BitmapDescriptor creating from drawable resources
    @NonNull
    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.marker_cluster_item,null);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Integer tag = (Integer) marker.getTag();

        return false;
    }

    @Override
    public void onCarSelection(int pos) {
        cView.onTransition();
        cView.selectedTransport(pos);
        cView.inidicatorTransformation(pos);

    }
}
