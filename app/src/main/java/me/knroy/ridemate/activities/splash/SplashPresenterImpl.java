package me.knroy.ridemate.activities.splash;

import android.os.Handler;

/**
 * Created by knroy on 6/3/17.
 * Don't edit or customize without proper privileges
 */

class SplashPresenterImpl implements SplashPresenter{

    private SplashView splashView;

    SplashPresenterImpl(SplashView splashView){
        this.splashView = splashView;
    }

    @Override
    public void splash() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(splashView != null){
                    splashView.navigateToMain();
                }
            }
        },2000L);

    }

    @Override
    public void onDestroy() {
        this.splashView = null;
    }
}
