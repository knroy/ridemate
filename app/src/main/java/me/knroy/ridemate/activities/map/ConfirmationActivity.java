package me.knroy.ridemate.activities.map;

import android.Manifest;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.transitionseverywhere.TransitionManager;

import me.knroy.ridemate.R;
import me.knroy.ridemate.exviews.AnimatedLinearLayout;
import me.knroy.ridemate.exviews.SquareImageView;
import me.knroy.ridemate.models.LocationTracker;

public class ConfirmationActivity extends AppCompatActivity implements OnMapReadyCallback,
        LocationDataListener, ConfirmationView, AnimatedLinearLayout.OnMenuClosedListener,View.OnClickListener {

    private static final int PERMISSION_REQUESTS = 100;

    private ImageView arrowButton;
    private ImageView locationButton;
    private ViewGroup transitionContainer;
    private LinearLayout arrowContainer;
    private Button confirmButton;
    private Spinner seats;
    private AnimatedLinearLayout chargeCalculation;
    private MapView mapView;

    private TextView locationAddress;

    private LocationTracker tracker;
    private ConfirmationPresenterImpl presenter;


    private TextView eta;
    private TextView charge;
    private TextView numberOfPeople;

    private RelativeLayout indicator_part1, indicator_part2, indicator_part3;

    private SquareImageView miniVan, sub, micro, bicycle, nova;

    private boolean isSelected;
    private int selectionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        isSelected = false;
        selectionId = 0;

        locationAddress = (TextView) findViewById(R.id.location_address);
        locationAddress.setText(R.string.loading);

        presenter = new ConfirmationPresenterImpl(this);

        mapView = (MapView) findViewById(R.id.pickup_map);

        mapView.onCreate(savedInstanceState);

        seats = (Spinner) findViewById(R.id.seat_spinner);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this, R.array.spinner_items, R.layout.item_spinner);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        seats.setAdapter(adapter);

        transitionContainer = (ViewGroup) findViewById(R.id.transition_container);

        arrowContainer = (LinearLayout) findViewById(R.id.arrow_container);
        arrowContainer.setOnClickListener(this);
        arrowButton = (ImageView) findViewById(R.id.imageArrow);

        eta = (TextView) findViewById(R.id.eta);
        charge = (TextView) findViewById(R.id.charge);
        numberOfPeople = (TextView) findViewById(R.id.peopleCapcity);

        miniVan = (SquareImageView) findViewById(R.id.mini_van_img);
        miniVan.setOnClickListener(this);

        sub = (SquareImageView) findViewById(R.id.sub_img);
        sub.setOnClickListener(this);

        micro = (SquareImageView) findViewById(R.id.micro_img);
        micro.setOnClickListener(this);

        bicycle = (SquareImageView) findViewById(R.id.cycle_img);
        bicycle.setOnClickListener(this);

        nova = (SquareImageView) findViewById(R.id.nova_img);
        nova.setOnClickListener(this);


        initChargeCalculation();

        // performing location tracking
        performTrackingOperation();

        confirmButton = (Button) findViewById(R.id.confirm_button);
        confirmButton.setOnClickListener(this);

        locationButton = (ImageView) findViewById(R.id.location_button);
        locationButton.setOnClickListener(this);

        initIndicator();
    }


    private void setWeight(RelativeLayout layout, int weight){
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
        params.weight = weight;
        layout.setLayoutParams(params);
    }

    private void initIndicator(){
        indicator_part1 = (RelativeLayout) findViewById(R.id.part_01);
        indicator_part2 = (RelativeLayout) findViewById(R.id.part_02);
        indicator_part3 = (RelativeLayout) findViewById(R.id.part_03);
        setWeight(indicator_part2,0);
    }



    private void initChargeCalculation(){
        chargeCalculation = (AnimatedLinearLayout) findViewById(R.id.charge_calculation);
        chargeCalculation.setSlider(false);
        chargeCalculation.measureHeight();
        chargeCalculation.setOnMenuClosedListener(this);
    }

    private void performTrackingOperation() {
        tracker = new LocationTracker(this);
        tracker.setListener(this);
        tracker.start(true);
        if (tracker.canGetLocation()) {
            onLocationChange(tracker.getLatLng());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        presenter.mapSync(this, googleMap, tracker.getLatLng());
    }

    @Override
    public void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_REQUESTS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUESTS) {
            if (grantResults.length > 0) {
                tracker.start(true);
            }
        } else {
            requestPermission();
        }
    }

    @Override
    public void requestGpsEnable() {
        presenter.requestGPSSettings(this);
    }

    @Override
    public void onLocationChange(LatLng latLng) {
        mapView.getMapAsync(this);
        presenter.geoUpdater(this, latLng);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tracker.onResume();
        try {
            mapView.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        tracker.onDestroy();
        presenter.onDestroy();
        try {
            mapView.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            mapView.onSaveInstanceState(outState);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapView.onLowMemory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        try {
            mapView.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        tracker.onDestroy();
        presenter.onDestroy();
        try {
            mapView.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }

    @Override
    public void updateGeo(String geoAddress) {
        locationAddress.setText(geoAddress);
    }

    @Override
    public void isLayoutVisible(boolean isVisible) {
        if (isVisible) {
            arrowButton.setImageDrawable(
                    ContextCompat.getDrawable(
                            ConfirmationActivity.this,
                            R.drawable.ic_arrow_up));
        } else {
            arrowButton.setImageDrawable(
                    ContextCompat.getDrawable(
                            ConfirmationActivity.this,
                            R.drawable.ic_arrow_down));
        }
    }

    @Override
    public void inidicatorTransformation(int pos) {
        if(!isSelected){
            setWeight(indicator_part2,0);
        }else{
            int leftWeight = pos - 1;
            int rightWight = 5 - pos;
            setWeight(indicator_part2,1);
            setWeight(indicator_part1,leftWeight);
            setWeight(indicator_part3,rightWight);
        }
    }

    private Drawable getBackgroundDrawable(){
        return isSelected ? ContextCompat.getDrawable(this, R.drawable.circle_blue) : ContextCompat.getDrawable(this, R.drawable.circle_white);
    }

    private Drawable getImageDrawable(int pos){
        int id;
        switch (pos){
            case 1:
                id =  isSelected ? R.drawable.mini : R.drawable.mini_invert;
                break;
            case 2:
                id =  isSelected ? R.drawable.sub : R.drawable.sub_invert;
                break;
            case 3:
                id =  isSelected ? R.drawable.micro : R.drawable.micro_invert;
                break;
            case 4:
                id =  isSelected ? R.drawable.bike : R.drawable.bike_invert;
                break;
            case 5:
                id =  isSelected ? R.drawable.nova : R.drawable.nova_invert;
                break;
            default:
                return null;
        }
        return ContextCompat.getDrawable(this, id);
    }

    private void changeImageSelectionColors(int pos){
        switch (pos){
            case 1:
                miniVan.setBackground(getBackgroundDrawable());
                miniVan.setImageDrawable(getImageDrawable(pos));
                break;
            case 2:
                sub.setBackground(getBackgroundDrawable());
                sub.setImageDrawable(getImageDrawable(pos));
                break;
            case 3:
                micro.setBackground(getBackgroundDrawable());
                micro.setImageDrawable(getImageDrawable(pos));
                break;
            case 4:
                bicycle.setBackground(getBackgroundDrawable());
                bicycle.setImageDrawable(getImageDrawable(pos));
                break;
            case 5:
                nova.setBackground(getBackgroundDrawable());
                nova.setImageDrawable(getImageDrawable(pos));
                break;
            default:
                break;
        }
    }

    @Override
    public void selectedTransport(int pos) {
        if(selectionId == pos){
            isSelected = !isSelected;
            selectionId = isSelected ? pos : 0;
            changeImageSelectionColors(pos);
        }else{
            isSelected = false;
            changeImageSelectionColors(selectionId);
            isSelected = !isSelected;
            selectionId = pos;
            changeImageSelectionColors(pos);
        }

        Log.d("TAG", "selectedTransport: " + selectionId + " " + pos);
    }

    @Override
    public void onTransition() {
        TransitionManager.beginDelayedTransition(transitionContainer);
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id) {
            case R.id.arrow_container:
                chargeCalculation.startAnimating();
                break;
            case R.id.confirm_button:
                break;
            case R.id.location_button:
                mapView.getMapAsync(this);
                presenter.geoUpdater(this, tracker.getLatLng());
                break;
            case R.id.mini_van_img:
                presenter.onCarSelection(1);
                break;
            case R.id.sub_img:
                presenter.onCarSelection(2);
                break;
            case R.id.micro_img:
                presenter.onCarSelection(3);
                break;
            case R.id.cycle_img:
                presenter.onCarSelection(4);
                break;
            case R.id.nova_img:
                presenter.onCarSelection(5);
                break;
            default:
                break;

        }
    }
}
