package me.knroy.ridemate.activities.map;

/**
 * Created by knroy on 6/8/17.
 * Don't edit or customize without proper privileges
 */

public interface ConfirmationView {
    void updateGeo(String geoAddress);
    void onTransition();
    void inidicatorTransformation(int pos);
    void selectedTransport(int pos);
}
