package me.knroy.ridemate.activities.splash;

/**
 * Created by knroy on 6/3/17.
 * Don't edit or customize without proper privileges
 */

interface SplashView {
    void navigateToMain();
}
