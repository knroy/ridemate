package me.knroy.ridemate.activities.map;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by knroy on 6/8/17.
 * Don't edit or customize without proper privileges
 */

public interface ConfirmationPresenter {
    void geoUpdater(Context context, LatLng latLng);
    void requestGPSSettings(Context context);
    void mapSync(Context context, GoogleMap map, LatLng latLng);
    void onDestroy();
    void onCarSelection(int pos);
}
