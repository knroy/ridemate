package me.knroy.ridemate.activities.main;

import me.knroy.ridemate.R;
import me.knroy.ridemate.fragments.clients.Clients;
import me.knroy.ridemate.fragments.demo.Demo;
import me.knroy.ridemate.fragments.home.Home;

/**
 * Created by knroy on 6/7/17.
 * Don't edit or customize without proper privileges
 */

class MainPresenterImpl implements MainPresenter {

    private MainView mainView;

    MainPresenterImpl(MainView mainView){
        this.mainView = mainView;
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void fragmentLoader(int pos) {

        String title = mainView.getActionbarString(getTitle(pos));
        mainView.setActionBarTitle(title);

        switch (pos){
            case R.id.nav_home:
                Home home = Home.getInstance();
                mainView.loadFragment(home);
                break;
            case R.id.nav_clients:
                Clients clients = Clients.getInstance();
                mainView.loadFragment(clients);
                break;
            default:
                Demo demo = Demo.getInstance(title);
                mainView.loadFragment(demo);
                break;
        }

        mainView.closeDrawer();
    }

    private int getTitle(int id){
        switch (id){
            case R.id.nav_home:
                return R.string.home;
            case R.id.nav_overview:
                return R.string.overview;
            case R.id.nav_portfolio:
                return R.string.portfolio;
            case R.id.nav_team:
                return R.string.team;
            case R.id.nav_clients:
                return R.string.clients;
            case R.id.nav_testimonials:
                return R.string.testimonial;
            case R.id.nav_methodology:
                return R.string.methodology;
            case R.id.nav_social:
                return R.string.social;
            case R.id.nav_faq:
                return R.string.faq;
            case R.id.nav_rate:
                return R.string.rate_this_app;
            case R.id.nav_privacy_policy:
                return R.string.privacy_policy;
            default:
                return R.string.home;
        }
    }

    @Override
    public void onExtraItemSelected(int id) {
        mainView.closeDrawer();
        switch (id){
            case R.id.nav_call:
                //mainView.onNavCall();
                mainView.makeToast("Clicked On Call");
                break;
            case R.id.nav_message:
                //mainView.onNavMail();
                mainView.makeToast("Clicked on Email");
                break;
            case R.id.nav_visit:
                //mainView.onNavVisit();
                mainView.makeToast("Clicked on Visit");
                break;
            default:
                break;
        }

    }
}
