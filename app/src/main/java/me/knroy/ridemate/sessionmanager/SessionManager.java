package me.knroy.ridemate.sessionmanager;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by knroy on 6/27/17.
 * Don't edit or customize without proper privileges
 */

public class SessionManager {

    private static final String PREF_NAME = "ridemate_pref";

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static SessionManager mInstance;

    private static final String KEY_REG_ID = "firebase_reg_id";

    private SessionManager(Context context){
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public static void start(Context context){
        if(mInstance == null){
            mInstance = new SessionManager(context);
        }
    }

    public static SessionManager getInstance(){
        if(mInstance == null)
            throw new NullPointerException();
        return mInstance;
    }

    public void setRegId(String regId){
        editor.putString(KEY_REG_ID, regId).apply();
    }

    public String getRegId(){
        return pref.getString(KEY_REG_ID, "");
    }



}
